package uk.ac.ebi.efo.bubastis;


import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.gfbio.config.ConfigSvc;
import org.semanticweb.owlapi.model.IRI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;


class Driver {

  private static final Logger LOGGER = LoggerFactory.getLogger(Driver.class);

  private static ConfigSvc configuration;

  /**
   * 
   * @param args
   * 
   *        args[0] ontology name (lowercased);
   *
   *        args[1] /path/to/ontology1.owl (current release);
   * 
   *        args[2] /path/to/ontology2.owl (latest release)
   */
  public static void main(String[] args) {

    String acronym = args[0];
    Instant start = Instant.now();
    // read GFBio configuration
    // GFBioConfiguration configuration = new GFBioConfiguration(args[0]);
    configuration = ConfigSvc.getInstance();
    configuration.setOntology(acronym);

    String labelIRI = configuration.getLabelProperty();
    IRI synIRI = configuration.getSynonymProperty();

    args = new String[] {"-ontology1", args[1], "-ontology2", args[2], "-output",
        configuration.getResultsPath() + "output_diff" + "_" + args[0] + ".xml", "-format", "xml"};

    Object ontologyObject1 = new Object();
    Object ontologyObject2 = new Object();

    // create options
    Options options = new Options();

    // locations of the two ontologies are required
    Option ontology1 = new Option("ontology1", true, "ontology 1 location");
    ontology1.setRequired(true);
    Option ontology2 = new Option("ontology2", true, "ontology 2 location");
    ontology2.setRequired(true);
    Option outputFile = new Option("output", true, "output file location");
    Option outputFormat = new Option("format", true, "output format");
    Option xsltPath = new Option("xslt", true, "location of xslt for xml output");

    options.addOption(ontology1);
    options.addOption(ontology2);
    options.addOption(outputFile);
    options.addOption(outputFormat);
    options.addOption(xsltPath);

    // if entityExpansionLimit hasn't already been set, set it
    // This is very important otherwise RDFXMLParser
    // fails with SAXParseException: The parser has encountered more
    // than "64,000" entity expansions
    // if (System.getProperty("entityExpansionLimit") == null) {
    // System.setProperty("entityExpansionLimit", "10000000");
    // }

    LOGGER.info("parsing commandline " + Arrays.toString(args));

    // parse arguments and do appropriate diff
    try {

      // create command line parser
      CommandLineParser parser = new DefaultParser();

      // parse the command line arguments
      CommandLine line = parser.parse(options, args);

      // for ontology 1 work out if this is a file or a url
      if (line.hasOption("ontology1")) {
        String ontology1location = line.getOptionValue("ontology1");

        // is ontology 1 a file location
        File file = new File(ontology1location);
        if (file.exists()) {
          System.out.println("ontology 1 is a file " + ontology1location);
          ontologyObject1 = new File(ontology1location);
        }
        // if not perhaps a url
        else {
          UrlValidator urlValidator = new UrlValidator();
          if (urlValidator.isValid(ontology1location)) {
            System.out.println("ontology 1 is a URL " + ontology1location);
            ontologyObject1 = ontology1location;
          } else {
            throw new ParseException("Ontology 1 is neither a file nor a URL");
          }
        }
      }

      // for ontology 2 work out if this is a file or a url
      if (line.hasOption("ontology2")) {
        String ontology2location = line.getOptionValue("ontology2");

        // is ontology 1 a file location
        File file = new File(ontology2location);
        if (file.exists()) {
          System.out.println("ontology 2 is a file " + ontology2location);
          ontologyObject2 = new File(ontology2location);
        }
        // if not perhaps a url
        else {
          UrlValidator urlValidator = new UrlValidator();
          if (urlValidator.isValid(ontology2location)) {
            System.out.println("ontology 2 is a URL " + ontology2location);
            ontologyObject2 = ontology2location;
          } else {
            throw new ParseException("Ontology 2 is neither a file nor a URL");
          }
        }
      }

      // do diff
      CompareOntologies comparer = new CompareOntologies(IRI.create(labelIRI), synIRI);
      if (ontologyObject1 instanceof String) {
        System.out.println("Ontology1 is a string");
        if (ontologyObject2 instanceof String) {
          // do diff with strings
          comparer.doFindAllChanges(ontologyObject1.toString(), ontologyObject2.toString());
        } else {
          // do diff with second ontology as file
          comparer.doFindAllChanges(ontologyObject1.toString(),
              new File(ontologyObject2.toString()));
        }
      }
      // ontology 1 is a file
      else {

        if (ontologyObject2 instanceof String) {
          // do diff with ontology 1 as file, ontology 2 as url
          comparer.doFindAllChanges(new File(ontologyObject1.toString()),
              ontologyObject2.toString());
        } else {
          // do diff with both files
          comparer.doFindAllChanges(new File(ontologyObject1.toString()),
              new File(ontologyObject2.toString()));


        }
      }


      // write results if a save file location was provided
      if (line.hasOption("output")) {
        String outputLocation = line.getOptionValue("output");

        // if a format was provided
        if (line.hasOption("format")) {
          String diffFormat = line.getOptionValue("format").toLowerCase();

          // and the format was xml
          if (diffFormat.matches("xml")) {

            // if an xslt location was provided
            if (line.hasOption("xslt")) {
              String xsltLocation = line.getOptionValue("xslt");

              // write xml results with xslt location to file
              comparer.writeDiffAsXMLFile(outputLocation, xsltLocation);
            } else {
              // write results without xslt location
              comparer.writeDiffAsXMLFile(outputLocation);

            }
          }
        }
        // otherwise use default rendering Manchester syntax in plain text
        else {
          comparer.writeDiffAsTextFile(outputLocation);

        }

      }


    } catch (ParseException exp) {
      // oops, something went wrong
      System.err.println("Parsing failed.  Reason: " + exp.getMessage());
    } catch (Exception e) {
      System.err.println("Performing diff failed.  Reason: " + e.getMessage());
    }

    Instant end = Instant.now();
    LOGGER.info("All done! Diff took " + formatDuration(Duration.between(start, end)));
    LOGGER.info("wrote result to " + configuration.getResultsPath() + "output_diff" + "_" + acronym
        + ".xml");

    if (Boolean.valueOf(configuration.getSettingFromINI("DIFF", "removeOldFiles")) == true) {
      try {
        FileUtils.forceDelete(new File(args[1]));
      } catch (IOException e) {
        LOGGER.error("failed to delete " + args[1] + " due to " + e.getLocalizedMessage());
      }
    }

    try {

      LOGGER.info("checking if XML is valid");
      File fXmlFile = new File(configuration.getResultsPath() + "output_diff_" + acronym + ".xml");
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(fXmlFile);
      if (doc != null)
        LOGGER.info("XML is valid");

      // 03/17/21 FB not needed for now
      // LOGGER.info("converting XML to JSON");
      //
      // List<String> command = new ArrayList<String>();
      // command.add("python3");
      // command.add(configuration.getWorkDir() + "XML2JSON.py");
      // command.add(fXmlFile.getAbsolutePath());
      // command.add(configuration.getResultsPath() + "output_diff_" + acronym + ".json");
      // command.add(configuration.getParameterFromGraph(acronym, "URI")
      // .replace(acronym.toLowerCase() + ".owl", ""));
      //
      // LOGGER.info("parsing command " + command);
      //
      // ProcessBuilder pb = new ProcessBuilder(command);
      // // pb.redirectErrorStream(true);
      // pb.inheritIO();
      // Process process = pb.start();
      //
      // int exitCode = process.waitFor();
      // if (exitCode == 0)
      // LOGGER.info("process completed");

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static String formatDuration(Duration duration) {
    long seconds = duration.getSeconds();
    long absSeconds = Math.abs(seconds);
    String positive =
        String.format("%d:%02d:%02d", absSeconds / 3600, (absSeconds % 3600) / 60, absSeconds % 60);
    return seconds < 0 ? "-" + positive : positive;
  }


}
