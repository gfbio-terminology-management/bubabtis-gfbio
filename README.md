# Bubastis

Bubastis is a tool for detecting asserted logical differences between two ontologies, such as between versions. 
 
# Availability
The original Bubastis is available as a web application at http://www.ebi.ac.uk/efo/bubastis/. There is documentation on how to use the Bubasists web
application here http://www.ebi.ac.uk/fgpt/sw/bubastis/index.html. 

# Building

The Bubasist Java library can be built using maven. 

`mvn clean package`

# Usage

It can either be used as a standalone tool or in a toolchain: `java -jar bubastis.jar`

Following arguments must be supplied (order is of importance):

> acronym path/to/current/version/.owl path/to/latest/version/.owl

Example:
>  ENVO /var/ts/ontologies/ENVO/2017-01-01/envo.owl /var/ts/ontologies/ENVO/2019-05-11/envo.owl 


Output per default is plain text. It will be saved under `/path/to/ontologies/acronym`, e.g., `/var/ts/ontologies/ITIS`
